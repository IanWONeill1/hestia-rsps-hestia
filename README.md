# Hestia

Open-source Kotlin MMO-RPG game server; emulating RS on October 2011, build 667.1

## Getting Started

Refer to our [Getting started guide](https://github.com/hestia-rsps/hestia/wiki/getting-started).

## Built With

* [Artemis-odb](https://github.com/junkdog/artemis-odb) - The game framework used
* [Gradle](https://gradle.org/) - Dependency Management
* [Netty](https://netty.io/) - Networking framework

## Contributing

Please read [CONTRIBUTING.md](CONTRIBUTING.md) for details on our code of conduct, and the process for submitting pull requests to us.

## Versioning

We use [SemVer](http://semver.org/) for versioning. For the versions available, see the [tags on this repository](https://github.com/hestia-rsps/hestia/tags).

## Authors

* **Greg Hibberd** - [GregHib](https://github.com/GregHib)

See also the list of [contributors](https://github.com/hestia-rsps/hestia/graphs/contributors) who participated in this project.

## License

This project is licensed under the BSD License - see the [LICENSE](LICENSE) file for details

## Acknowledgments

* [Major](https://www.rune-server.ee/members/major/) & [Peterbjornx](https://www.rune-server.ee/members/peterbjornx/) for client refactor's
* The [apollo](https://github.com/apollo-rsps/apollo) team for inspiration & layouts
* [Tyluur](https://www.rune-server.ee/members/tyluur/)'s [RedRune project](https://www.rune-server.ee/runescape-development/rs-503-client-server/projects/654209-redrune-667-beta-tba.html) cause I've occasionally peaked at his code
* [Divergent](https://github.com/rsJuuuuu/Divergent667) data references
* [Matrix 830](https://www.rune-server.ee/runescape-development/rs-503-client-server/downloads/618618-matrix-3-official-release-recommended-rs3-download-830-a.html) cache data
* [Admiral slee](https://www.rune-server.ee/members/admiral+slee/) for [displee.com](https://displee.com/archive/)
