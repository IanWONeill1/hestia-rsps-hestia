package worlds.gregs.hestia.game.update.blocks.mob

import worlds.gregs.hestia.game.update.UpdateBlock

data class TransformBlock(override val flag: Int, val mobId: Int) : UpdateBlock