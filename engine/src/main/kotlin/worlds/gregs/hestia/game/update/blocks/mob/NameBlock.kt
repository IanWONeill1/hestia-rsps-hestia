package worlds.gregs.hestia.game.update.blocks.mob

import worlds.gregs.hestia.game.update.UpdateBlock

data class NameBlock(override val flag: Int, val name: String) : UpdateBlock