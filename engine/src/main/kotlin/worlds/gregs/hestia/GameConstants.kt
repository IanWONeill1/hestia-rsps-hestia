package worlds.gregs.hestia

object GameConstants {
    const val PLAYERS_LIMIT = 2048
    const val MOB_LIMIT = 40000
}