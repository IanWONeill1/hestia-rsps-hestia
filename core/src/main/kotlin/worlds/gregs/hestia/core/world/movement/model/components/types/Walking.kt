package worlds.gregs.hestia.core.world.movement.model.components.types

import com.artemis.Component
import com.artemis.annotations.PooledWeaver

@PooledWeaver
class Walking : Component()