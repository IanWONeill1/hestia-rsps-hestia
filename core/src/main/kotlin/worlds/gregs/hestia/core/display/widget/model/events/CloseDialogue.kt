package worlds.gregs.hestia.core.display.widget.model.events

import worlds.gregs.hestia.artemis.InstantEvent

data class CloseDialogue(val entityId: Int) : InstantEvent