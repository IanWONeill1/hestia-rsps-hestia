package worlds.gregs.hestia.core.display.widget.model.components

import com.artemis.Component
import com.artemis.annotations.PooledWeaver

@PooledWeaver
abstract class FullScreenWidget : Component()