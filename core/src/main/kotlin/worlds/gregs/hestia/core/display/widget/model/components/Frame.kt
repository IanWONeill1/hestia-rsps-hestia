package worlds.gregs.hestia.core.display.widget.model.components

import com.artemis.Component

abstract class Frame : Component()