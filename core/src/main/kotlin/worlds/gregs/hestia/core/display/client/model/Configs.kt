package worlds.gregs.hestia.core.display.client.model

object Configs {
    /* Combat styles */
    const val COMBAT_STYLE = 43
    /* Stats */
    const val SKILL_STAT_FLASH = 1179
    const val LEVEL_UP_DETAILS = 1230
    const val SKILL_MENU = 965
    /* Quest Journals */
    const val QUEST_POINTS = 101
    const val UNSTABLE_FOUNDATIONS_QUEST = 281
    /* Prayer List */
    const val PRAYER_POINTS = 2382
    const val CURSES = 1584
    /* Emotes */
    const val GOBLIN_QUEST_EMOTES = 465
    const val STRONGHOLD_SECURITY_EMOTES = 802
    const val HALLOWEEN_EMOTES = 1085
    const val EVENT_EMOTES = 313
}