package worlds.gregs.hestia.core.display.dialogue.model.events

import net.mostlyoriginal.api.event.common.Event

data class StringEntered(val entityId: Int, val string: String): Event