package worlds.gregs.hestia.core.entity.mob.api

import worlds.gregs.hestia.core.entity.entity.api.EntityChunk

abstract class MobChunk : EntityChunk(Mob::class)