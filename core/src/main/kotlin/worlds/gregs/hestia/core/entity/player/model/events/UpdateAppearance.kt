package worlds.gregs.hestia.core.entity.player.model.events

import net.mostlyoriginal.api.event.common.Event

data class UpdateAppearance(val entityId: Int): Event