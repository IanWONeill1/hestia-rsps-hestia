package worlds.gregs.hestia.core.display.dialogue.model.events

import net.mostlyoriginal.api.event.common.Event

data class IntegerEntered(val entityId: Int, val integer: Int): Event